package sys.db;

class Postgres {
    public static function connect(params:{ user : String, ?socket : Null<String>, ?port : Null<Int>, pass : String, host : String, ?database : Null<String> }):Connection {
        if (params.database == null)
            params.database = 'postgres';
        if (params.port == null)
            params.port = 5432;

        var pgl = new PostgresLow();
        pgl.connectSync('postgresql://${params.user}:${params.pass}@${params.host}:${params.port}/${params.database}');
        return new PostgresConnection(pgl);
    }
}

class PostgresConnection implements Connection {
    var pgl:PostgresLow;

    public function new(pgl:PostgresLow) {
        this.pgl = pgl;
    }

    public function addValue(s:StringBuf, v:Dynamic):Void {
        if (Std.is(v, Int) || v == null) {
            s.add(v);
        } else if (Std.is(v, Bool)) {
            s.add(v ? 1 : 0);
        } else if (Std.is(v, Date)) {
            s.add(quote((v.getUTCMonth()+1) + " " + (v.getUTCDate()) + " " + v.getUTCFullYear() + " " + v.getUTCHours() + ':' + v.getUTCMinutes() + ":" + v.getUTCSeconds()));
        } else {
            s.add(quote(Std.string(v)));
        }
    }

    public function close() {
        pgl.close();
    }

    public function commit() {
        request('COMMIT');
    }

    public function dbName() {
        return 'Postgres';
    }

    public function escape(s:String):String {
        return null;
    }

    public function lastInsertId():Int {
        var answer:Int;
        var res = pgl.querySync('SELECT LASTVAL() as id');
        return res[0].id;
    }

    public function quote(s:String):String {
        //TODO: FIX
        if (s.indexOf("\000") >= 0)
            return "x'" + haxe.Utf8.encode(s) + "'";
        return "'" + s + "'";
    }

    public function request(s:String):ResultSet {
        trace(s);
        var res = pgl.querySync(s);
        return new PostgresResultSet(res);
    }

    public function rollback() {
        request('ROLLBACK');
        return null;
    }

    public function startTransaction() {
        request('BEGIN TRANSACTION');
        return null;
    }
}

@:jsRequire('pg-native')
extern class PostgresLow {
    public function new();
    public function connectSync(params:String):Void;
    public function querySync(query:String):Dynamic;
    public function close():Void;
}

class PostgresResultSet implements ResultSet {
    private var _results:Array<Dynamic>;
    private var iterator:Iterator<Dynamic>;

    public function new(results) {
        this._results = results;
        this.iterator = _results.iterator();
    }

	public var length(get, null):Int;
    function get_length() {
        return _results.length;
    } 

	public var nfields(get, null):Int;
    function get_nfields() {
        return Reflect.fields(_results[0]).length;
    }

	public function hasNext():Bool {
        return iterator.hasNext();
    }

	public function next():Dynamic {
        return iterator.next();
    }

	public function results():List<Dynamic> {
        return Lambda.list(_results);
    }

	public function getResult(n:Int):String {
        throw 'Not implemented';
    }

	public function getIntResult(n:Int):Int {
        trace('The N is ${n}');
        trace('Initial value is');
        trace(_results);
        return Std.parseInt(_results[n]);
    }

	public function getFloatResult(n:Int):Float {
        trace('The N is ${n}');
        trace('Initial value is');
        trace(_results);
        return Std.parseFloat(_results[n]);
    }

	public function getFieldsNames():Null<Array<String>> {
        return Reflect.fields(_results[0]);
    }
}